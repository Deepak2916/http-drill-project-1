const http = require('http')

const UUId = require('crypto')

const { PORT } = require('./config')


http.createServer((req, res) => {
  const url = req.url.toLowerCase()
  res.setHeader('Content-Type', 'text/html')

  if (url === '/html') {

    res.end(`<!DOCTYPE html>
        <html>
          <head>
          </head>
          <body>
              <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
              <p> - Martin Fowler</p>
        
          </body>
        </html>`);
  }
  else if (url === '/json') {
    res.setHeader('Content-Type', 'application/json')

    const givenObjectData = {
      "slideshow": {
        "author": "Yours Truly",
        "date": "date of publication",
        "slides": [
          {
            "title": "Wake up to WonderWidgets!",
            "type": "all"
          },
          {
            "items": [
              "Why <em>WonderWidgets</em> are great",
              "Who <em>buys</em> WonderWidgets"
            ],
            "title": "Overview",
            "type": "all"
          }
        ],
        "title": "Sample Slide Show"
      }
    }
    res.end(JSON.stringify(givenObjectData))
  }
  else if (url === '/uuid') {

    res.setHeader('Content-Type', 'application/json')

    const uuid4 = UUId.randomUUID()
    res.end(JSON.stringify({
      "uuid": uuid4
    }))
  }
  else if (url === '/status') {
    res.setHeader('Content-Type', 'application/json')

    res.end(`{
      "message": "use below status codes to test",
      "valid_status_codes":${JSON.stringify(http.STATUS_CODES)},

    }`)
  }
  else if (url.startsWith('/status')) {
    res.setHeader('Content-Type', 'application/json')

    const params = url.split('/')

    const status = params[params.length - 1]
    const statusCode = http.STATUS_CODES[status]

    if (http.STATUS_CODES[status] && params.length === 3) {

      res.statusCode = status
      const output = `
      {
        "status code": "${status}",
        "message": "${statusCode}"
      }`
      res.end(output)

    } else {

      res.statusCode = 400

      const invalidendPoint = `{
        "status code": "Invalid",
        "message": "please enter valid status code"
      }`
      res.end(invalidendPoint)

    }


  }
  else if (url.startsWith('/delay')) {
    res.setHeader('Content-Type', 'application/json')

    const params = url.split('/')
    const seconds = +params[params.length - 1]
    if (typeof seconds === 'number' && params.length === 3 && seconds >= 0) {

      setTimeout(() => {
        res.statusCode = 200
        res.end(`
        {
          "second": seconds,
          "message": "delayed ${seconds} seconds"
        }`)
      }, seconds * 1000)
    } else {
      res.statusCode = 400
      res.end(`
      {
        "second": "invalid",
        "message": "please enter valid seconds(>=0)"
      }`)
    }

  }

  else {

    res.setHeader('Content-Type', 'text/html')
    let message;
    let statusCode;
    let color;
    if (url === '/') {
      statusCode = 200
      message = 'HOME PAGE'
      color = 'color:green'
    } else {
      statusCode = 400
      message = 'Invalid end point choose one of the below end points'
      color = 'color:red'
    }
    res.statusCode = statusCode
    res.end(`<!DOCTYPE html>
    <html>
      <head>
      </head>
      <body>
          <h1 style=${color}>${message}</h1>
          <a href='/html'> - /html</a><br>
          <a href='/json'> - /json </a><br>
          <a href='/uuid'> - /uuid</a><br>
          <a href='/status'> - /status/{status-code}</a><br>
          <a href='/delay/0'> - /delay/{delay_in_seconds}</a><br>
    
      </body>
    </html>`)
  }

})
  .listen(PORT, () => {
    console.log(`server is running on port`)
  })